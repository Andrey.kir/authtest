<?php

class Route
{
    const MAIN_DATA_FILENAME = "app/model/MainData.php";
    const MAIN_TEMPLATE_FILENAME = "app/controller/MainLoader.php";

    /**
     * @return void
     */
    static public function start()
    {
        $loginError = false;

        if (file_exists(self::MAIN_DATA_FILENAME)) {
            include self::MAIN_DATA_FILENAME;
        }

        $auth = new AuthLogic();

        if (isset($_POST['via']) && $_POST['via'] == 'logout') {
            $auth->logout();
        }

        $isLogin = $auth->isLogin();
        
        if (isset($_POST['via']) && $_POST['via'] == 'login') {
            $loginError = $auth->login();
            if ($loginError === false) {
                $isLogin = true;
            }
        }

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1]) ) {
            $baseControllerName = $routes[1];

            if ($baseControllerName == "error404") {
                include "app/controller/Error404Loader.php";
                $error404Loader = new Error404Loader();
                $error404Loader->actionLoad(false);
            } else {
                Route::page404();
            }

        } else {
            Route::pageMain($isLogin, $loginError);
        }
    }

    /**
     * @return void
     */
    static private function page404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . 'error404');
    }

    /**
     * @param $isLogin
     * @param $loginError
     */
    static private function pageMain($isLogin, $loginError)
    {
        if (file_exists(self::MAIN_TEMPLATE_FILENAME)) {
            include self::MAIN_TEMPLATE_FILENAME;
        } else {
            Route::page404();
        }

        $mainLoader = new MainLoader();
        $mainLoader->actionLoad($isLogin, $loginError);
    }
}
