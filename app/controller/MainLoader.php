<?php

class MainLoader extends Controller
{
    /**
     * @return void
     */
    public function actionLoad($isLogin = false, $loginError = false)
    {
        if ($isLogin) {

            if(isset($_COOKIE["login"])) {
                $data['username'] = htmlspecialchars($_COOKIE["login"]);
            } else {
                $data['username'] = htmlspecialchars($_POST["name"]);
            }

            $this->view->generate('hello.php', 'mainTemplate.php', $data);
        } else {
            $data['error'] = $loginError;
            $this->view->generate('login.php', 'mainTemplate.php', $data);
        }
    }
}
