<?php

class AuthLogic
{
    const EXPIRE_BLOCK_TIME = 300;
    const EXPIRE_COOKIE_TIME = 300;

    /**
     * @var MainData
     */
    private $model;

    /**
     * @var string[][]|void
     */
    private $users;

    /**
     * AuthLogic constructor.
     */
    public function __construct()
    {
        $this->model = new MainData();
        $this->users = $this->model->getUsersData();
    }

    /**
     * @return bool
     */
    public function isLogin()
    {
        session_start();
        if ($_SESSION['auth'] && isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
            return true;
        }
        session_destroy();
        return false;
    }

    /**
     * @return bool|string
     */
    public function login()
    {
        if ($_POST['name'] != "" && $_POST['password'] != "") {
            $login = $_POST['name'];
            $password = $_POST['password'];

            if ($this->spamBlock()) {
                if (isset($_COOKIE['spamblocktime'])) {
                    $expiryTime = $_COOKIE['spamblocktime'] - time();
                } else {
                    $expiryTime = self::EXPIRE_BLOCK_TIME;
                }

                return "Попробуйте еще раз через $expiryTime секунд";
            }

            foreach ($this->users AS $user) {
                $user_details = explode('|', $user);
                if ($user_details[0] == $login && trim($user_details[1]) == md5($password)) {
                    setcookie("login", $login, time() + self::EXPIRE_COOKIE_TIME);
                    setcookie("password", $password, time() + self::EXPIRE_COOKIE_TIME);
                    session_start();
                    $_SESSION['auth'] = true;
                    return false;
                }
            }

            return "Неверные Данные";
        } else {
            return "Поля не должны быть пустыми!";
        }
    }

    /**
     * @return void
     */
    public function logout()
    {
        session_start();
        session_destroy();
        setcookie("login", 1, time() - 1);
        setcookie("password", 1, time() - 1);
        setcookie("spamblocktime", 1, time() - 1);
        setcookie("spamcounter", 1, time() - 1);
    }

    /**
     * @return bool
     */
    private function spamBlock()
    {
        if (isset($_COOKIE['spamblocktime'])) {
            return true;
        }

        if (!isset($_COOKIE['spamcounter'])) {
            setcookie("spamcounter", 1, time() + self::EXPIRE_COOKIE_TIME);
            return false;
        } else {
            $spanCounter = $_COOKIE['spamcounter'];
            $spanCounter++;
            if ($spanCounter > 3) {
                $expiryTime = time() + self::EXPIRE_BLOCK_TIME;
                setcookie("spamblocktime", $expiryTime, $expiryTime);
                return true;
            } else {
                setcookie("spamcounter", 1, time() - 1);
                setcookie("spamcounter", $spanCounter, time() + self::EXPIRE_COOKIE_TIME);
                return false;
            }
        }
    }
}