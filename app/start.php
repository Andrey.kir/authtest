<?php

require_once 'core/View.php';
require_once 'controller/AuthLogic.php';
require_once 'core/Controller.php';
require_once 'core/Route.php';

Route::start();
